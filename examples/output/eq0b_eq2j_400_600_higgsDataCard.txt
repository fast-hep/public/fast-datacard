imax    *     number of categories
jmax    *     number of samples minus one
kmax    *     number of nuisance parameters
-------------------------------------------------------------------------------
shapes * * eq0b_eq2j_400_600_shapes.root $CHANNEL/$PROCESS $CHANNEL/$PROCESS_$SYSTEMATIC

bin	 Signal	DoubleMu	SingleMu	
observation	205	169	101	
-------------------------------------------------------------------------------
bin	Signal	Signal	Signal	Signal	DoubleMu	DoubleMu	SingleMu	SingleMu	

process	 VBF	Zinv	Ttw	Qcd	VBF	Ewk	VBF	Ewk	
process	 0	1	2	3	0	1	0	1	
rate	 81.7435798645	91.061466217	71.9923782349	86.0621261597	1e-12	33.7388572693	1e-12	11.9153308868	
-------------------------------------------------------------------------------
ElectronSFWeight shape	1	-	-	-	1	-	1	-	
bosonPtEwkWeight shape	1	1	1	1	1	1	1	1	
bosonPtQcdWeight shape	1	1	1	1	1	1	1	1	
bosonPtWeight shape	1	1	1	1	1	1	1	1	
bsfCFbWeight shape	1	-	-	-	1	-	1	-	
bsfCFcWeight shape	1	-	-	-	1	-	1	-	
bsfCFlWeight shape	1	-	-	-	1	-	1	-	
bsfLightWeight shape	1	1	1	1	1	1	1	1	
bsfWeight shape	1	1	1	1	1	1	1	1	
jecWeight shape	-	1	1	1	-	1	-	1	
lumiSyst lnN	1.026	1.06	-	-	1.026	1.06	1.026	1.06	
muonSfWeight shape	1	1	1	1	1	1	1	1	
muonTrackWeight shape	1	-	-	-	1	-	1	-	
nIsrWeight shape	1	1	1	1	1	1	1	1	
photonSFWeight shape	1	1	1	1	1	1	1	1	
photonTriggerWeight shape	1	1	1	1	1	1	1	1	
puWeight shape	1	1	1	1	1	1	1	1	
triggerWeight shape	1	1	1	1	1	1	1	1	
xsWeightTt shape	1	1	1	1	1	1	1	1	
xsWeightW shape	1	1	1	1	1	1	1	1	
