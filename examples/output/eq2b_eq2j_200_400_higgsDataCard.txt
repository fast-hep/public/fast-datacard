imax    *     number of categories
jmax    *     number of samples minus one
kmax    *     number of nuisance parameters
-------------------------------------------------------------------------------
shapes * * eq2b_eq2j_200_400_shapes.root $CHANNEL/$PROCESS $CHANNEL/$PROCESS_$SYSTEMATIC

bin	 Signal	SingleMu	
observation	169	205	
-------------------------------------------------------------------------------
bin	Signal	Signal	Signal	Signal	SingleMu	SingleMu	

process	 VBF	Zinv	Ttw	Qcd	VBF	Ewk	
process	 0	1	2	3	0	1	
rate	 30.9939193726	68.3467559814	46.8153686523	82.8264923096	1e-12	92.8051300049	
-------------------------------------------------------------------------------
ElectronSFWeight shape	1	-	-	-	1	-	
bosonPtEwkWeight shape	1	1	1	1	1	1	
bosonPtQcdWeight shape	1	1	1	1	1	1	
bosonPtWeight shape	1	1	1	1	1	1	
bsfCFbWeight shape	1	-	-	-	1	-	
bsfCFcWeight shape	1	-	-	-	1	-	
bsfCFlWeight shape	1	-	-	-	1	-	
bsfLightWeight shape	1	1	1	1	1	1	
bsfWeight shape	1	1	1	1	1	1	
jecWeight shape	-	1	1	1	-	1	
lumiSyst lnN	1.026	1.06	-	-	1.026	1.06	
muonSfWeight shape	1	1	1	1	1	1	
muonTrackWeight shape	1	-	-	-	1	-	
nIsrWeight shape	1	1	1	1	1	1	
photonSFWeight shape	1	1	1	1	1	1	
photonTriggerWeight shape	1	1	1	1	1	1	
puWeight shape	1	1	1	1	1	1	
triggerWeight shape	1	1	1	1	1	1	
xsWeightTt shape	1	1	1	1	1	1	
xsWeightW shape	1	1	1	1	1	1	
